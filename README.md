# Cracked Castle mod

This mod brings you 5 new blocks, 2 new ladders, and one cobweb to help you build a ruined castle or fortress! As a Bonus feature we also included a King-Sam skin!

-- Old Stone Bricks:
If you are making your castle walls out of stonebrick, then i suggest trying out the *Broken Stonebrick* and the *Mossy Stonebrick* blocks.

-- Old Stone Blocks:
Making a floor out of stone-blocks is always a good idea but if you are working on an old castle then mix in some *Broken Stone Block* and some *Mossy Stone Block*!

-- Fire Cobble:
Place down *Fire Cobble* along with some ignated Coal blocks to make it look like your castle got hit.

-- Ladders:
Ladders gets broken very quickly if you dont look after them, thats what should happen at your castle too! If you use ladders then why not to replace one or two with the new *Broken Ladders*? They come in both variant!

-- Cobweb:
And our favourite block in all castles, the *Cobweb*! If you place it on a wall or on the roof, the cobweb will automaticly connect to it like a real one... The possibilites are endless!

--Version:
*1.0*

--License for the code:
*LGPl 2.1*

--License for the textures:
*CC--BY-SA 4.0*

--Depencies:
*stairs* (hard(shipped by default)), *default* (optional(shipped by default)), *farming* (optional(shipped by default)).
