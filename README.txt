Description:
This mod gives you 5 new blocks, two ladders, a cobweb and of course all the stairs and slabs for the stonebricks and for the stone-blocks.
You can craft most of them by placing 8 def. material in a "circle" around an item.

Crafting the Mossy Stonebrick, Mosyy Stone Block and the two Old ladder:
def.material def.material def.material
def.material dirt def.material
def.material def.material def.material

Crafting the Cracked Stonebrick and the Cracked Stone Block
def.material def.material def.material
def.material wooden pickaxe def.material
def.material def.material def.material

Crafting the Fire Cobblestone
cobble cobble cobble
cobble torch cobble
cobble cobble cobble

Crafting the Cobweb
string string string
string nothing string
string string string


Depencies: default (Optional)
License for code: LGPL 2.1
License for textures: CC--BY-SA 4.0
original textures and codes comes from default and from stairs
Version: 0.2
