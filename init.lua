-- Registering simple nodes = line 9
-- Registering ladders = line 56
-- Registering cobweb = line 105
-- Crating recipies = line 120
-- Registering slabs and stairs = line 193
-- Unimportant stuff = 235


----------------- Registering Simple Nodes -----------------
-- Cracked stonebrick
minetest.register_node("cracked_castle:cracked_stonebrick", {
	description = ("Cracked Stone Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"default_stone_brick.png^cracked_castle_cracked_stonebrick.png"},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})
-- Mossy stonebrick
minetest.register_node("cracked_castle:mossy_stonebrick", {
	description = ("Mossy Stone Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"default_stone_brick.png^cracked_castle_mossy_stonebrick.png"},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})
-- Cracked stone block
minetest.register_node("cracked_castle:cracked_stone_block", {
	description = ("Cracked Stone Block"),
	tiles = {"default_stone_block.png^cracked_castle_cracked_stone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})
-- Mossy stone block
minetest.register_node("cracked_castle:mossy_stone_block", {
	description = ("Mossy Stone Block"),
	tiles = {"default_stone_block.png^cracked_castle_mossy_stone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})
-- Fire cobble
minetest.register_node("cracked_castle:fire_cobble", {
	description = ("Fire Cobblestone"),
	tiles = {"default_cobble.png^cracked_castle_fire_cobble.png"},
	is_ground_content = false,
	groups = {cracky = 3, stone = 2},
	sounds = default.node_sound_stone_defaults(),
})


----------------- Registering Ladders -----------------
-- Old Wooden Ladder
minetest.register_node("cracked_castle:old_wooden_ladder", {
	description = ("Old Wooden Ladder"),
	drawtype = "signlike",
	tiles = {"default_ladder_wood.png^cracked_castle_old_wooden_ladder.png"},
	inventory_image = "default_ladder_wood.png^cracked_castle_old_wooden_ladder.png",
	wield_image = "default_ladder_wood.png^cracked_castle_old_wooden_ladder.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	climbable = true,
	is_ground_content = false,
	selection_box = {
		type = "wallmounted",
		--wall_top = = <default>
		--wall_bottom = = <default>
		--wall_side = = <default>
	},
	groups = {choppy = 2, oddly_breakable_by_hand = 3, flammable = 2},
	legacy_wallmounted = true,
	sounds = default.node_sound_wood_defaults(),
})

-- Old Steel Ladder
minetest.register_node("cracked_castle:old_steel_ladder", {
	description = ("Old Steel Ladder"),
	drawtype = "signlike",
	tiles = {"default_ladder_steel.png^cracked_castle_old_steel_ladder.png"},
	inventory_image = "default_ladder_steel.png^cracked_castle_old_steel_ladder.png",
	wield_image = "default_ladder_steel.png^cracked_castle_old_steel_ladder.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	climbable = true,
	is_ground_content = false,
	selection_box = {
		type = "wallmounted",
		--wall_top = = <default>
		--wall_bottom = = <default>
		--wall_side = = <default>
	},
	groups = {cracky = 2},
	sounds = default.node_sound_metal_defaults(),
})


----------------- Registering the Cobweb -----------------
minetest.register_node("cracked_castle:cobweb", {
    description = ("Cobweb"),
    drawtype = "firelike",
    tiles = {"cobweb_by_extex.png"}, -- Thank you to Extex for the Amazing texture
	inventory_image = "cobweb_by_extex.png", -- If you dont like the texture because it got too much lines then change it here from
    wield_image = "cobweb_by_extex.png", -- "cobweb_by_extex.png" to "cracked_castle_cobweb_OLD.png"
    walkable = false,
    paramtype = "light",
    sunlight_propagates = true,
    groups = {snappy=3, flammable=3},
	sounds = default.node_sound_leaves_defaults()
})


----------------- Crafting Recipies -----------------
-- Crafting old stonebricks
minetest.register_craft({
    output = "cracked_castle:cracked_stonebrick 8",
    recipe = {
        {"default:stonebrick", "default:stonebrick", "default:stonebrick"},
        {"default:stonebrick", "default:pick_wood", "default:stonebrick"},
        {"default:stonebrick", "default:stonebrick", "default:stonebrick"}
    }
})

minetest.register_craft({
    output = "cracked_castle:mossy_stonebrick 8",
    recipe = {
        {"default:stonebrick", "default:stonebrick", "default:stonebrick"},
        {"default:stonebrick", "default:dirt", "default:stonebrick"},
        {"default:stonebrick", "default:stonebrick", "default:stonebrick"}
    }
})
-- Crafting old stone blocks
minetest.register_craft({
    output = "cracked_castle:cracked_stone_block 8",
    recipe = {
        {"default:stone_block", "default:stone_block", "default:stone_block"},
        {"default:stone_block", "default:pick_wood", "default:stone_block"},
        {"default:stone_block", "default:stone_block", "default:stone_block"}
    }
})
minetest.register_craft({
    output = "cracked_castle:mossy_stone_block 8",
    recipe = {
        {"default:stone_block", "default:stone_block", "default:stone_block"},
        {"default:stone_block", "default:dirt", "default:stone_block"},
        {"default:stone_block", "default:stone_block", "default:stone_block"}
    }
})
-- Crafting fire cobble
minetest.register_craft({
    output = "cracked_castle:fire_cobble 8",
    recipe = {
        {"default:cobble", "default:cobble", "default:cobble"},
        {"default:cobble", "default:torch", "default:cobble"},
        {"default:cobble", "default:cobble", "default:cobble"}
    }
})
-- Crafting old ladders
minetest.register_craft({
    output = "cracked_castle:old_wooden_ladder 8",
    recipe = {
        {"default:ladder", "default:ladder", "default:ladder"},
        {"default:ladder", "default:dirt", "default:ladder"},
        {"default:ladder", "default:ladder", "default:ladder"}
    }
})
minetest.register_craft({
    output = "cracked_castle:old_steel_ladder 8",
    recipe = {
        {"default:steel_ladder", "default:steel_ladder", "default:steel_ladder"},
        {"default:steel_ladder", "default:dirt", "default:steel_ladder"},
        {"default:steel_ladder", "default:steel_ladder", "default:steel_ladder"}
    }
})
-- Crafting Cobweb
minetest.register_craft({
    output = "cracked_castle:cobweb 7",
    recipe = {
        {"farming:string", "farming:string", "farming:string"},
        {"farming:string", "", "farming:string"},
        {"farming:string", "farming:string", "farming:string"}
    }
})


----------------- Registering Slabs and Stairs -----------------
stairs.register_stair_and_slab(
    "cracked_stonebrick",
    "cracked_castle:cracked_stonebrick",
    {cracky = 2, stone = 1},
    {"default_stone_brick.png^cracked_castle_cracked_stonebrick.png"},
    ("Cracked Stonebrick Stair"),
    ("Cracked Stonebrick Slab"),
    default.node_sound_stone_defaults(),
    false
)
stairs.register_stair_and_slab(
    "mossy_stonebrick",
    "cracked_castle:mossy_stonebrick",
    {cracky = 2, stone = 1},
    {"default_stone_brick.png^cracked_castle_mossy_stonebrick.png"},
    ("Mossy Stonebrick Stair"),
    ("Mossy Stonebrick Slab"),
    default.node_sound_stone_defaults(),
    false
)
stairs.register_stair_and_slab(
    "cracked_stone_block",
    "cracked_castle:cracked_stone_block",
    {cracky = 2, stone = 1},
    {"default_stone_block.png^cracked_castle_cracked_stone_block.png"},
    ("Cracked Stone Block Stair"),
    ("Cracked Stone Block Slab"),
    default.node_sound_stone_defaults(),
    false
)
stairs.register_stair_and_slab(
    "mossy_stone_block",
    "cracked_castle:mossy_stone_block",
    {cracky = 2, stone = 1},
    {"default_stone_block.png^cracked_castle_mossy_stone_block.png"},
    ("Mossy Stone Block Stair"),
    ("Mossy Stone Block Slab"),
    default.node_sound_stone_defaults(),
    false
)

----------------- Unimportant stuff -----------------
-- Secret Block
minetest.register_node("cracked_castle:secret_block", {
	description = ("IM DA SECRET BLOCK!!"),
	tiles = {"cracked_castle_cracked_stonebrick_OLD.png"},
	is_ground_content = false,
	groups = {oddly_breakable_by_hand=1, not_in_creative_inventory=1},
})

---- BONUS: King Sam skin!
--You have to use a skin mod (i suggest using SkinsDB...),
--and then you have to copy the bonus skin into the mod (the mod tells you most of the time what you have to do to get custom skins)
